/*#include <iostream>
#include "CancerData.h"

class AVLNode
{
private:
    CancerData data;
    AVLNode *leftPtr;
    AVLNode *rightPtr;
    int nodeHeight;

public:
    AVLNode();

    AVLNode(AVLNode &newObject);

    AVLNode(CancerData nData, AVLNode *nLPtr, AVLNode *nRPtr, int nHeight);

    ~AVLNode();

    AVLNode* getLeftPtr();

    AVLNode* getRightPtr();

    CancerData getData();

    int getNodeHeight();

    void setLeftPtr(AVLNode *nLeftPtr);

    void setRightPtr(AVLNode *nRightPtr);

    void setData(CancerData nData);

    void setNodeHeight(int nHeight);

};


AVLNode::AVLNode()
{
    leftPtr = nullptr;

    rightPtr = nullptr;

    nodeHeight = 0;
}

AVLNode::AVLNode(AVLNode &newObject)
{
    data = newObject.getData();

    leftPtr = newObject.getLeftPtr();

    rightPtr = newObject.getRightPtr();

    nodeHeight = newObject.getNodeHeight();
}

AVLNode::AVLNode(CancerData nData, AVLNode *nLPtr, AVLNode *nRPtr, int nHeight)
{
    data = nData;

    leftPtr = nLPtr;

    rightPtr = nRPtr;

    nodeHeight = nHeight;
}

AVLNode::~AVLNode()
{
    delete leftPtr;

    delete rightPtr;
}

AVLNode* AVLNode::getLeftPtr()
{
    return leftPtr;
}

AVLNode* AVLNode::getRightPtr()
{
    return rightPtr;
}

CancerData AVLNode::getData()
{
    return data;   
}

int AVLNode::getNodeHeight()
{
    return nodeHeight;
}

void AVLNode::setLeftPtr(AVLNode *nLeftPtr)
{
    leftPtr = nLeftPtr;
}

void AVLNode::setRightPtr(AVLNode *nRightPtr)
{
    rightPtr = nRightPtr;
}

void AVLNode::setData(CancerData nData)
{
    data = nData;
}

void AVLNode::setNodeHeight(int nHeight)
{
    nodeHeight = nHeight;
}*/