/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 3                                              *
* February 29, 2020                                                     *
* Decription: This file contains class declaration for the CancerData   *
*             class.                                                    *
************************************************************************/

#pragma once
#include <string>
#include <iostream>

class CancerData
{

private:

    std::string countryName;

    float cancerRate;

public:

    CancerData(); //Default constructor

    CancerData(std::string nCountryName, float nCancerRate); //Explicit constructor

    CancerData(CancerData& newObject); //Copy constructor

    ~CancerData(); //Destructor

    std::string getCountryName(); //Standard getter for country name

    float getCancerRate(); //Standard getter for cancer rate

    void setCountryName(std::string nCountryName); //Standard setter for country name

    void setCancerRate(float nCancerRate); //Standard setter for cancer rate

};


