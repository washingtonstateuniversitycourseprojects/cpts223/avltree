/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 3                                              *
* February 29, 2020                                                     *
* Decription: This file contains class declaration and member function  *
*             definitionsfor the AVLNode class.                         *
************************************************************************/

#pragma once
#include <iostream>
#include <string>
#include "CancerData.h"


template <class T>

class AVLNode
{
private:
    T data;
    AVLNode* leftPtr = nullptr;
    AVLNode* rightPtr = nullptr;
    int nodeHeight;

public:
    AVLNode();

    AVLNode(AVLNode<T>& newObject);

    AVLNode(T nData, AVLNode<T>* nLPtr, AVLNode<T>* nRPtr, int nHeight);

    ~AVLNode();

    AVLNode<T>*& getLeftPtr();

    AVLNode<T>*& getRightPtr();

    T getData();

    int getNodeHeight();

    void setLeftPtr(AVLNode<T>* nLeftPtr);

    void setRightPtr(AVLNode<T>* nRightPtr);

    void setData(T nData);

    void setNodeHeight(int nHeight);

    void setNodeHeight();
};


/************************************************************************
* Programming Assignment 3                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object of type AVLNode.                    *
************************************************************************/

template <class T>
AVLNode<T>::AVLNode()
{
    leftPtr = nullptr;

    rightPtr = nullptr;

    nodeHeight = 0;
}

/************************************************************************
* Programming Assignment 3                                              *
* Copy constructor:                                                     *
*                                                                       *
* Description: Constructs an object of type AVLNode using given         *
*              another object of type AVLNode.                          *
************************************************************************/

template <class T>
AVLNode<T>::AVLNode(AVLNode<T>& newObject)
{
    data = newObject.getData();

    leftPtr = newObject.getLeftPtr();

    rightPtr = newObject.getRightPtr();

    nodeHeight = newObject.getNodeHeight();
}


/************************************************************************
* Programming Assignment 3                                              *
* Standard constructor:                                                 *
*                                                                       *
* Description: Constructs an object of type AVLNode using given         *
*              member variables.                                        *
************************************************************************/

template <class T>
AVLNode<T>::AVLNode(T nData, AVLNode<T>* nLPtr, AVLNode<T>* nRPtr, int nHeight)
{
    data = nData;

    leftPtr = nLPtr;

    rightPtr = nRPtr;

    nodeHeight = nHeight;
}


/************************************************************************
* Programming Assignment 3                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destructs object of type AVLNode.                        *
************************************************************************/

template <class T>
AVLNode<T>::~AVLNode()
{
    
}


/************************************************************************
* Programming Assignment 3                                              *
* GetLeftPtr:                                                           *
*                                                                       *
* Description: Returns the member variable leftPtr.                     *
************************************************************************/

template <class T>
AVLNode<T>*& AVLNode<T>::getLeftPtr()
{
    return leftPtr;
}


/************************************************************************
* Programming Assignment 3                                              *
* GetRightPtr:                                                          *
*                                                                       *
* Description: Returns the member variable rightPtr.                    *
************************************************************************/

template <class T>
AVLNode<T>*& AVLNode<T>::getRightPtr()
{
    return rightPtr;
}


/************************************************************************
* Programming Assignment 3                                              *
* GetData:                                                              *
*                                                                       *
* Description: Returns the member variable data.                        *
************************************************************************/

template <class T>
T AVLNode<T>::getData()
{
    return data;
}


/************************************************************************
* Programming Assignment 3                                              *
* GetNodeHeight:                                                        *
*                                                                       *
* Description: Returns the member variable nodeHeight.                  *
************************************************************************/

template <class T>
int AVLNode<T>::getNodeHeight()
{
    return nodeHeight;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetLeftPtr:                                                           *
*                                                                       *
* Description: Sets the member variable leftPtr.                        *
************************************************************************/

template <class T>
void AVLNode<T>::setLeftPtr(AVLNode* nLeftPtr)
{
    leftPtr = nLeftPtr;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetRightPtr:                                                          *
*                                                                       *
* Description: Sets the member variable rightPtr.                       *
************************************************************************/

template <class T>
void AVLNode<T>::setRightPtr(AVLNode* nRightPtr)
{
    rightPtr = nRightPtr;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetData:                                                              *
*                                                                       *
* Description: Sets the member variable data.                           *
************************************************************************/

template <class T>
void AVLNode<T>::setData(T nData)
{
    data = nData;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetNodeHeight:                                                        *
*                                                                       *
* Description: Sets the member variable nodeHeight                      *
************************************************************************/

template <class T>
void AVLNode<T>::setNodeHeight(int nHeight)
{
    nodeHeight = nHeight;
}

/************************************************************************
* Programming Assignment 3                                              *
* SetNodeHeight:                                                        *
*                                                                       *
* Description: Sets the member variable nodeHeight using its children.  *
************************************************************************/

template <class T>
void AVLNode<T>::setNodeHeight()
{
    if ((getLeftPtr() == nullptr) && (getRightPtr() == nullptr)) //Node has no children
    {
        setNodeHeight(0);
    }
    else
    {
        int leftSubtreeHeight, rightSubtreeHeight;

        if (getRightPtr()) //Node has a right child
        {
            rightSubtreeHeight = getRightPtr()->getNodeHeight();
        }
        else //Node has no right child
        {
            rightSubtreeHeight = -1;
        }

        if (getLeftPtr()) //Node has a left child
        {
            leftSubtreeHeight = getLeftPtr()->getNodeHeight();
        }
        else //Node has no left child
        {
            leftSubtreeHeight = -1;
        }

        if (rightSubtreeHeight <= leftSubtreeHeight) //If left subtree has greater height
        {
            setNodeHeight(leftSubtreeHeight + 1); //Set node height to +1 of larger subtree height
        }
        else if (rightSubtreeHeight >= leftSubtreeHeight) //If right subtree has greater height
        {
            setNodeHeight(rightSubtreeHeight + 1); //Set node height to +1 of larger subtree height
        }
    }
}
