/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 3                                              *
* February 29, 2020                                                     *
* Decription: This file contains class declaration and member function  *
*             definitionsfor the AVLTree class.                         *
************************************************************************/

#pragma once
#include "AVLNode.h"
#include <fstream>
#include "CancerData.h"
#include <queue>
#include <string>
#include <iomanip>


template <class T>
class AVLTree
{

private:

    AVLNode<T>* root; //Private data memer root

public:

    AVLTree(); //Default constructor

    AVLTree(AVLTree& newObject); //Copy constructor

    AVLTree(AVLNode<T>* nRoot); //Standard constructor

    ~AVLTree(); //Destructor

    bool isEmpty();

    void loadData(std::ifstream& fileStream);

    int height();

    void insert(AVLNode<T>*& nodePtr, AVLNode<T>*& nNode, int prevMove, int lastMove);

    void remove(AVLNode<T>*& deleteNode, AVLNode<T>*& nodePtr, int prevMove, int lastMove);

    void clear(AVLNode<T>*& nodePtr);

    AVLNode<T>* findMin(AVLNode<T>* nodePtr);

    AVLNode<T>* findMax(AVLNode<T>* nodePtr);

    void printInOrder(AVLNode<T>* nodePtr);

    void printBackwardsOrder(AVLNode<T>* nodePtr, std::string label, int &rankNumber);

    AVLNode<T>* getRoot();

    void setRoot(AVLNode<T>* nRoot);

    void rotateLeft(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr);

    void rotateRight(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr);

    void rotateLeftRight(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr);

    void rotateRightLeft(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr);

    void printLevelOrder(AVLNode<T>* root);
};


/************************************************************************
* Programming Assignment 3                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object of type AVLTree.                    *
************************************************************************/

template <class T>
AVLTree<T>::AVLTree()
{

}


/************************************************************************
* Programming Assignment 3                                              *
* Copy constructor:                                                     *
*                                                                       *
* Description: Constructs an object of type AVLTree using given         *
*              another object of type AVLTree.                          *
************************************************************************/

template <class T>
AVLTree<T>::AVLTree(AVLTree<T>& newObject)
{
    root = newObject.getRoot();
}


/************************************************************************
* Programming Assignment 3                                              *
* Standard constructor:                                                 *
*                                                                       *
* Description: Constructs an object of type AVLTree using given         *
*              member variable root.                                    *
************************************************************************/

template <class T>
AVLTree<T>::AVLTree(AVLNode<T>* nRoot)
{
    root = nRoot;
}


/************************************************************************
* Programming Assignment 3                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destructs object of type AVLTree by calling clear.       *
************************************************************************/

template <class T>
AVLTree<T>::~AVLTree()
{
    clear(root);
}


/************************************************************************
* Programming Assignment 3                                              *
* isEmpty:                                                              *
*                                                                       *
* Description: Returns true if AVLTree is empty.                        *
************************************************************************/

template <class T>
bool AVLTree<T>::isEmpty()
{
    return (root == nullptr);
}


/************************************************************************
* Programming Assignment 3                                              *
* loadData:                                                             *
*                                                                       *
* Description: Loads data from a file into AVLTree.                     *
************************************************************************/

template <class T>
void AVLTree<T>::loadData(std::ifstream& fileStream)
{
    std::string newName, throwaway, newRate; //Initializes variables to be placed in new data in tree

    float newRateFloat; //Initializes variable to be placed in new data in tree

    AVLNode<T>* newNode; //Initializes new AVLNode for tree

    CancerData newData; //Initializes data to store in new AVLNode for tree

    int prevMove = -1, lastMove = -1; //Initializes variables to track moves in case of rotations

    while (!fileStream.eof()) //Loops until end of file
    {
        newNode = new AVLNode<T>; //Make new node for each set of data

        std::getline(fileStream, newName, ','); //Reads in data until comma in infile

        if (newName == "") //If name read in is empty
        {
            std::getline(fileStream, newName, ','); //Read in data until comma of infile
        }

        if (newName == "") //If hame read in is still empty
        {
            break; //End of file has been reached, break out of loop
        }

        newData.setCountryName(newName); //Set read in name to data variable

        std::getline(fileStream, newRate); //Read in data until end of line

        newRateFloat = std::stof(newRate); //Convert read in data to float

        newData.setCancerRate(newRateFloat); //Set read in data to data variable

        newNode->setData(newData); //Set node data to new data

        insert(root, newNode, prevMove, lastMove); //Insert new node with new data into tree
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* Height:                                                               *
*                                                                       *
* Description: Returns height of AVLTree.                               *
************************************************************************/

template <class T>
int AVLTree<T>::height()
{
    if (root)
    {
        return root->getNodeHeight();
    }
    else
    {
        return -1;
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* Insert:                                                               *
*                                                                       *
* Description: Inserts new node into AVLTree, rotating as needed.       *
************************************************************************/

template <class T>
void AVLTree<T>::insert(AVLNode<T>*& nodePtr, AVLNode<T>*& nNode, int prevMove, int lastMove)
{
    std::cout << std::endl;

    if (nodePtr == nullptr) //If subtree is empty
    {
        nodePtr = nNode; //Set subtree with new Node

        nodePtr->setNodeHeight(); //Set node height after insertion
    }
    else if (*nNode < *nodePtr) //If data in new node is less than current node
    {
        prevMove = 0; //Node is being inserted to the left

        if ((nodePtr->getLeftPtr()) && (*nNode < *(nodePtr->getLeftPtr()))) //Find following move
        {
            lastMove = 0; //Node is being inserted to the left, left
        }
        else if ((nodePtr->getLeftPtr()) && (*nNode >= * (nodePtr->getLeftPtr()))) //Find following move
        {
            lastMove = 1; //Node is being inserted to the left, right
        }

        insert(nodePtr->getLeftPtr(), nNode, prevMove, lastMove); //Insert node in left subtree

        nodePtr->setNodeHeight(); //Set node height after insertion
    }
    else if (*nNode >= * nodePtr) //If data in new node is greater than than current node
    {
        prevMove = 1; //Node is being inserted to the right

        if ((nodePtr->getRightPtr()) && (*nNode < *(nodePtr->getRightPtr()))) //Find following move
        {
            lastMove = 0; //Node is being inserted to the right, left
        }
        else if ((nodePtr->getRightPtr()) && (*nNode >= * (nodePtr->getRightPtr()))) //Find following move
        {
            lastMove = 1; //Node is being inserted to the right, right
        }

        insert(nodePtr->getRightPtr(), nNode, prevMove, lastMove); //Insert node in right subtree

        nodePtr->setNodeHeight(); //Set node height after insertion
    }


    if (nodePtr->getLeftPtr() && nodePtr->getRightPtr()) //If current node has two children (possible imbalance)
    {
        if (abs(nodePtr->getLeftPtr()->getNodeHeight() - nodePtr->getRightPtr()->getNodeHeight()) > 1) //Imbalance is found
        {
            if ((prevMove == 0) && (lastMove == 0)) //Left, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left grandchild
            }
            else if ((prevMove == 1) && (lastMove == 1)) //Right, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right grandchild
            }
            else if ((prevMove == 0) && (lastMove == 1)) //Left, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeftRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left-right grandchild
            }
            else if ((prevMove == 1) && (lastMove == 0)) //Right, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRightLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right-left grandchild
            }
        }
    }
    else if (nodePtr->getLeftPtr()) //If current node has left child (possible imbalance)
    {
        if (abs(nodePtr->getLeftPtr()->getNodeHeight() - (0 - 1)) > 1) //Imbalance is found
        {
            if ((prevMove == 0) && (lastMove == 0)) //Left, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left grandchild
            }
            else if ((prevMove == 1) && (lastMove == 1)) //Right, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right grandchild
            }
            else if ((prevMove == 0) && (lastMove == 1)) //Left, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeftRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left-right grandchild
            }
            else if ((prevMove == 1) && (lastMove == 0)) //Right, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRightLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right-left grandchild
            }
        }
    }
    else if (nodePtr->getRightPtr()) //If current node has right child (possible imbalance)
    {
        if (abs(nodePtr->getRightPtr()->getNodeHeight() - (0 - 1)) > 1) //Imbalance is found
        {
            if ((prevMove == 0) && (lastMove == 0)) //Left, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left grandchild
            }
            else if ((prevMove == 1) && (lastMove == 1)) //Right, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right grandchild
            }
            else if ((prevMove == 0) && (lastMove == 1)) //Left, right insertion
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeftRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left-right grandchild
            }
            else if ((prevMove == 1) && (lastMove == 0)) //Right, left insertion
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRightLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right-left grandchild
            }
        }
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* Remove:                                                               *
*                                                                       *
* Description: Removes a node into AVLTree, rotating as needed.         *
************************************************************************/

template <class T>
void AVLTree<T>::remove(AVLNode<T>*& deleteNode, AVLNode<T>*& nodePtr, int prevMove, int lastMove)
{
    std::cout << std::endl;

    int leftSubtreeHeight, rightSubtreeHeight; //Initialize subtree heights to compare to find imbalances

    if (!nodePtr) //If node to delete is not found
    {
        return; //Exit
    }

    if ((*deleteNode - *nodePtr) <= (0 - 0.1)) //If node to delete is less than current node
    {
        remove(deleteNode, nodePtr->getLeftPtr()); //Call remove with left child of current node

        nodePtr->setNodeHeight(); //Set node height after removal
    }
    else if ((*deleteNode - *nodePtr) >= 0.1) //If node to delete is greater than current node
    {
        remove(deleteNode, nodePtr->getRightPtr()); //Call remove with right child of current node

        nodePtr->setNodeHeight(); //Set node height after removal
    }
    else //If node to delete is current node
    {
        if (nodePtr->getLeftPtr() && nodePtr->getRightPtr()) //If current node has two children
        {
            nodePtr->setData(findMin(nodePtr->getRightPtr())->getData()); //Set current node data (data being deleted) to minimum of right subtree

            remove(nodePtr, nodePtr->getRightPtr()); //Call remove with right child of current node, deleting minimum of right subtree that has been set to current node

            nodePtr->setNodeHeight(); //Set node height after deletion
        }
        else //current node does not have two children
        {
            AVLNode<T>* temp = nodePtr; //Set temp to delete

            if (nodePtr->getLeftPtr()) //If current node has left child
            {
                nodePtr = nodePtr->getLeftPtr(); //Set current node to left child
            }
            else //Else if current node has right child
            {
                nodePtr = nodePtr->getRightPtr(); //Set current node to right child
            }

            delete temp; //Make deletion
        }
    }

    if (nodePtr) //If current node wasn't deleted
    {

        if (nodePtr->getLeftPtr()) //If current node has left child
        {
            leftSubtreeHeight = nodePtr->getLeftPtr()->getNodeHeight(); //Left subtree height is height of left child
        }
        else //If current node does not have left child
        {
            leftSubtreeHeight = -1; //Left subtree does not exist, height is -1 for nullptr
        }

        if (nodePtr->getRightPtr()) //If current node has right child
        {
            rightSubtreeHeight = nodePtr->getRightPtr()->getNodeHeight(); //Right subtree height is height of right child
        }
        else //If current node does not have right child
        {
            rightSubtreeHeight = -1; //Right subtree does not exist, height is -1 for nullptr
        }
    }


    if (!nodePtr) //If node pointer does not exist, do nothing
    {

    }
    else if (!nodePtr->getLeftPtr() && !nodePtr->getRightPtr()) //If nodePtr does not have any children, do nothing, no imbalance exists
    {

    }
    else if (abs(leftSubtreeHeight - rightSubtreeHeight) > 1) //If an imbalance exists
    {
        if (leftSubtreeHeight > rightSubtreeHeight) //If imbalance comes from left subtree
        {

            if (nodePtr->getLeftPtr()) //If left child exists
            {

                if (nodePtr->getLeftPtr()->getLeftPtr()) //If left child has left child
                {
                    leftSubtreeHeight = nodePtr->getLeftPtr()->getLeftPtr()->getNodeHeight(); //Set left subtree of left child height
                }
                else
                {
                    leftSubtreeHeight = -1; //Set left subtree of left child height
                }

                if (nodePtr->getLeftPtr()->getRightPtr()) //If left child has right child
                {
                    rightSubtreeHeight = nodePtr->getLeftPtr()->getRightPtr()->getNodeHeight(); //Set right subtree of left child height
                }
                else
                {
                    rightSubtreeHeight = -1; //Set right subtree of left child height
                }
            }

            if (leftSubtreeHeight > rightSubtreeHeight) //If imbalance requires a right rotation
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left-left grandchild
            }
            else //If imbalance requires a left-right rotation
            {
                AVLNode<T>* childPtr = nodePtr->getLeftPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeftRight(nodePtr, childPtr, newPtr); //Make rotation using left child and left-right grandchild
            }
        }
        else if (rightSubtreeHeight > leftSubtreeHeight) //If imbalance comes from right subtree
        {

            if (nodePtr->getRightPtr()) //If right child exists
            {

                if (nodePtr->getRightPtr()->getLeftPtr()) //If right child has left child
                {
                    leftSubtreeHeight = nodePtr->getRightPtr()->getLeftPtr()->getNodeHeight(); //Set left subtree of right child height
                }
                else //If right child does not have left child
                {
                    leftSubtreeHeight = -1; //Set left subtree of right child height
                }

                if (nodePtr->getRightPtr()->getRightPtr()) //If right child has right child
                {
                    rightSubtreeHeight = nodePtr->getRightPtr()->getRightPtr()->getNodeHeight(); //Set right subtree of right child height
                }
                else //If right child does not have right child
                {
                    rightSubtreeHeight = -1; //Set right subtree of right child height
                }
            }

            if (leftSubtreeHeight < rightSubtreeHeight) //If imbalance requires left rotation
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getRightPtr(); //Set new pointer used in rotation

                rotateLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right-right grandchild
            }
            else //If imbalance requires right-left rotation
            {
                AVLNode<T>* childPtr = nodePtr->getRightPtr(); //Set child pointer used in rotation

                AVLNode<T>* newPtr = childPtr->getLeftPtr(); //Set new pointer used in rotation

                rotateRightLeft(nodePtr, childPtr, newPtr); //Make rotation using right child and right-left grandchild
            }
        }
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* Clear:                                                                *
*                                                                       *
* Description: Recursively deletes every node in the tree.              *
************************************************************************/

template <class T>
void AVLTree<T>::clear(AVLNode<T>*& nodePtr)
{
    if (nodePtr) //If current node exists
    {
        if (nodePtr->getLeftPtr()) //If left child of current node exists
        {
            clear(nodePtr->getLeftPtr()); //Clear left subtree
        }
        if (nodePtr->getRightPtr()) //If right child of current node exists
        {
            clear(nodePtr->getRightPtr()); //Clear right subtree
        }

        delete nodePtr; //Delete current node
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* FindMin:                                                              *
*                                                                       *
* Description: Recursively finds the smallest node in the tree.         *
************************************************************************/

template <class T>
AVLNode<T>* AVLTree<T>::findMin(AVLNode<T>* nodePtr)
{
    if (nodePtr->getLeftPtr()) //If nodes exist to the left of current node
    {
        return findMin(nodePtr->getLeftPtr()); //Recursively call findMin with left subtree
    }
    else
    {
        return nodePtr; //Return smallest node
    }

}


/************************************************************************
* Programming Assignment 3                                              *
* FindMax:                                                              *
*                                                                       *
* Description: Recursively finds the largest node in the tree.          *
************************************************************************/

template <class T>
AVLNode<T>* AVLTree<T>::findMax(AVLNode<T>* nodePtr)
{
    if (nodePtr->getRightPtr()) //If nodes exist to the right of current node
    {
        return findMax(nodePtr->getRightPtr()); //Recursively call findMax with right subtree
    }
    else
    {
        return nodePtr; //Return largest node
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* PrintInOrder:                                                         *
*                                                                       *
* Description: Prints all nodes in increasing order.                    *
************************************************************************/

template <class T>
void AVLTree<T>::printInOrder(AVLNode<T>* nodePtr)
{
    if (nodePtr) //If current node exists
    {
        printInOrder(nodePtr->getLeftPtr()); //Recursively call printInOrder with left subtree

        std::cout << nodePtr << std::endl; //Print current node

        printInOrder(nodePtr->getRightPtr()); //Recursively call printInOrder with right subtree
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* PrintBackwardsOrder:                                                  *
*                                                                       *
* Description: Prints all nodes in decreasing order.                    *
************************************************************************/

template <class T>
void AVLTree<T>::printBackwardsOrder(AVLNode<T>* nodePtr, std::string label, int &rankNumber)
{
    if (nodePtr) //If current node exists
    {
        printBackwardsOrder(nodePtr->getRightPtr(), label, rankNumber); //Recursively call printBackwardsOrder with right subtree

        std::cout << "<" << label << ": " << rankNumber++ << ". " << nodePtr << ">" << std::endl; //Print current node

        printBackwardsOrder(nodePtr->getLeftPtr(), label, rankNumber); //Recursively call printBackwardsOrder with left subtree
    }

}


/************************************************************************
* Programming Assignment 3                                              *
* getRoot:                                                              *
*                                                                       *
* Description: Returns the root of the AVLTree.                         *
************************************************************************/

template <class T>
AVLNode<T>* AVLTree<T>::getRoot()
{
    return root;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetRoot:                                                              *
*                                                                       *
* Description: Sets the root of the AVLTree to another AVLNode.         *
************************************************************************/

template <class T>
void AVLTree<T>::setRoot(AVLNode<T>* nRoot)
{
    root = nRoot;
}


/************************************************************************
* Programming Assignment 3                                              *
* RotateRight:                                                          *
*                                                                       *
* Description: Balances a tree after a left-left insertion.             *
************************************************************************/

template <class T>
void AVLTree<T>::rotateRight(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr)
{
    parentPtr->setLeftPtr(childPtr->getRightPtr()); //Reset parent pointer's pointer to child pointer to avoid circular links

    childPtr->setRightPtr(parentPtr); //Reset child pointer's pointer to new pointer to avoid circular links

    parentPtr->setNodeHeight(); //Set height after editing parent pointer

    newPtr->setNodeHeight(); //Set height after editing new pointer

    childPtr->setNodeHeight(); //Set height after editing child pointer

    parentPtr = childPtr; //Finish rotation
}


/************************************************************************
* Programming Assignment 3                                              *
* RotateLeft:                                                           *
*                                                                       *
* Description: Balances a tree after a right-right insertion.           *
************************************************************************/

template <class T>
void AVLTree<T>::rotateLeft(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr)
{
    parentPtr->setRightPtr(childPtr->getLeftPtr()); //Reset parent pointer's pointer to child pointer to avoid circular links

    childPtr->setLeftPtr(parentPtr); //Reset child pointer's pointer to new pointer to avoid circular links

    parentPtr->setNodeHeight(); //Set height after editing parent pointer

    newPtr->setNodeHeight(); //Set height after editing new pointer

    childPtr->setNodeHeight(); //Set height after editing child pointer

    parentPtr = childPtr; //Finish rotation
}


/************************************************************************
* Programming Assignment 3                                              *
* RotateLeftRight:                                                      *
*                                                                       *
* Description: Balances a tree after a left-right insertion.            *
************************************************************************/

template <class T>
void AVLTree<T>::rotateLeftRight(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr)
{
    parentPtr->setLeftPtr(newPtr->getRightPtr()); //Reset parent pointer's pointer to child pointer to avoid circular links

    childPtr->setRightPtr(newPtr->getLeftPtr()); //Reset child pointer's pointer to new pointer to avoid circular links

    newPtr->setLeftPtr(childPtr); //Set left child of new pointer to child pointer

    newPtr->setRightPtr(parentPtr); //Set right child of new pointer to parent pointer

    parentPtr->setNodeHeight(); //Set height after editing parent pointer

    childPtr->setNodeHeight(); //Set height after editing child pointer

    newPtr->setNodeHeight(); //Set height after editing new pointer

    parentPtr = newPtr; //Finish rotation
}


/************************************************************************
* Programming Assignment 3                                              *
* RotateRightLeft:                                                      *
*                                                                       *
* Description: Balances a tree after a right-left insertion.            *
************************************************************************/

template <class T>
void AVLTree<T>::rotateRightLeft(AVLNode<T>*& parentPtr, AVLNode<T>*& childPtr, AVLNode<T>*& newPtr)
{
    parentPtr->setRightPtr(newPtr->getLeftPtr()); //Reset parent pointer's pointer to child pointer to avoid circular links

    childPtr->setLeftPtr(newPtr->getRightPtr()); //Reset child pointer's pointer to new pointer to avoid circular links

    newPtr->setRightPtr(childPtr); //Set right child of new pointer to child pointer

    newPtr->setLeftPtr(parentPtr); //Set left child of new pointer to parent pointer

    parentPtr->setNodeHeight(); //Set height after editing parent pointer

    childPtr->setNodeHeight(); //Set height after editing child pointer

    newPtr->setNodeHeight(); //Set height after editing new pointer

    parentPtr = newPtr; //Finish rotation
}


/************************************************************************
* Programming Assignment 3                                              *
* PrintLevelOrder:                                                      *
*                                                                       *
* Description: Prints all nodes of tree by level.                       *
************************************************************************/

template <class T>
void AVLTree<T>::printLevelOrder(AVLNode<T>* root)
{
    std::cout << std::endl;

    int i = 1; //Initialize a count to record number of nodes

    std::queue<AVLNode<T>*> NodeQueue; //Initialize a queue

    AVLNode<T>* curPtr; //Initialize a pointer to current queue pointer

    NodeQueue.push(root); //Push root onto queue

    NodeQueue.push(nullptr); //Push nullptr onto queue

    while (NodeQueue.size() > 1) //Loop until queue is empty
    {
        curPtr = NodeQueue.front(); //Set current pointer

        NodeQueue.pop(); //Pop current pointer of queue

        if (curPtr == nullptr) //If current pointer is null
        {
            NodeQueue.push(nullptr); //Push a new nullptr onto the queue

            std::cout << "\n"; //Print out a new line
        }
        else //If current pointer is not null
        {
            if (curPtr->getLeftPtr()) //If current pointer has a left child
            {
                NodeQueue.push(curPtr->getLeftPtr()); //Add left child to queue
            }
            if (curPtr->getRightPtr()) //If current pointer has right child
            {
                NodeQueue.push(curPtr->getRightPtr()); //Add right child to queue
            }

            std::cout << i++ << ": " << curPtr << " "; //Increment count
        }
    }
}


/************************************************************************
* Programming Assignment 3                                              *
* Overloaded < operator:                                                *
*                                                                       *
* Description: Compares two objects of type AVLNode<CancerData>.        *
************************************************************************/

bool operator<(AVLNode<CancerData>& lhs, AVLNode<CancerData>& rhs)
{
    return lhs.getData().getCancerRate() < rhs.getData().getCancerRate();
}


/************************************************************************
* Programming Assignment 3                                              *
* Overloaded >= operator:                                               *
*                                                                       *
* Description: Compares two objects of type AVLNode<CancerData>.        *
************************************************************************/

bool operator>=(AVLNode<CancerData>& lhs, AVLNode<CancerData>& rhs)
{
    return lhs.getData().getCancerRate() >= rhs.getData().getCancerRate();
}


/************************************************************************
* Programming Assignment 3                                              *
* Overloaded == operator:                                               *
*                                                                       *
* Description: Compares two objects of type AVLNode<CancerData>.        *
************************************************************************/

bool operator==(AVLNode<CancerData> lhs, AVLNode<CancerData> rhs)
{
    return lhs.getData().getCancerRate() == rhs.getData().getCancerRate();
}


/************************************************************************
* Programming Assignment 3                                              *
* Overloaded - operator:                                                *
*                                                                       *
* Description: Subtracts two objects of type AVLNode<CancerData>.       *
************************************************************************/

bool operator-(AVLNode<CancerData>& lhs, AVLNode<CancerData>& rhs)
{
    return lhs.getData().getCancerRate() - rhs.getData().getCancerRate();
}

/************************************************************************
* Programming Assignment 3                                              *
* Overloaded << operator:                                               *
*                                                                       *
* Description: Prints an object of type AVLNode<CancerData>.            *
************************************************************************/

std::ostream& operator<<(std::ostream& filestream, AVLNode<CancerData>* nodePtr)
{
    filestream << nodePtr->getData().getCountryName();

    filestream << ". " << std::setprecision(5) << (nodePtr->getData().getCancerRate());

    return filestream;
}
