/*#include "PracticeAVLNode.h"
#include <fstream>


class AVLTree
{

private:

    AVLNode *root;

public:

    AVLTree();

    AVLTree(AVLTree &newObject);

    AVLTree(AVLNode *nRoot);

    ~AVLTree();

    bool isEmpty();

    void loadData(std::ifstream &fileStream, AVLTree &dataTree);

    int height();

    void insert(AVLNode *&nodePtr, AVLNode *&nNode, int &prevMove, int &lastMove);

    void remove(AVLNode *&deleteNode, AVLNode *&nodePtr, int &prevMove, int &lastMove);

    void clear(AVLNode *nodePtr);

    AVLNode* findMin(AVLNode *& nodePtr);

    AVLNode* findMax(AVLNode *& nodePtr);

    void printInOrder(AVLNode *nodePtr);

    AVLNode* getRoot();

    void setRoot(AVLNode *nRoot);

    void rotateLeft(AVLNode *&parentPtr);

    void rotateRight(AVLNode *&parentPtr);

};


AVLTree::AVLTree()
{

}

AVLTree::AVLTree(AVLTree &newObject)
{
    root = newObject.getRoot();
}

AVLTree::AVLTree(AVLNode *nRoot)
{
    root = nRoot;
}

AVLTree::~AVLTree()
{
    clear(root);
}

bool AVLTree::isEmpty()
{
    return (root == nullptr);
}

void AVLTree::loadData(std::ifstream &fileStream, AVLTree &dataTree)
{
    std::string newName, throwaway, newRate;

    float newRateInt;

    AVLNode *newNode;

    CancerData newData;

    int prevMove = -1, lastMove = -1;

    while (!fileStream.eof())
    {
        newNode = new AVLNode;

        std::getline(fileStream, newName, ',');

        if (newName == "")
        { 
            std::getline(fileStream, newName, ',');
        }

        newData.setCountryName(newName);

        std::getline(fileStream, newRate);

        newRateInt = std::stof(newRate);

        newData.setCancerRate(newRateInt);

        newNode->setData(newData);

        dataTree.insert(root, newNode, prevMove, lastMove);
    }
}

int AVLTree::height()
{
    return root->getNodeHeight();
}

void AVLTree::insert(AVLNode *&nodePtr, AVLNode *&nNode, int &prevMove, int &lastMove)
{

    if (nodePtr == nullptr)
    {
        nodePtr = nNode;

        if ((nodePtr->getLeftPtr() == nullptr) && (nodePtr->getRightPtr() == nullptr))
        {
            nodePtr->setNodeHeight(0);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() > nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getLeftPtr()->getNodeHeight() + 1);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() < nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getRightPtr()->getNodeHeight() + 1);
        }
    }
    else if (nNode->getData().getCancerRate() < nodePtr->getData().getCancerRate())
    {
        prevMove = lastMove;

        lastMove = 0; //Node is being inserted to the left

        insert(nodePtr->getLeftPtr(), nNode, prevMove, lastMove);
        
        if ((nodePtr->getLeftPtr() == nullptr) && (nodePtr->getRightPtr() == nullptr))
        {
            nodePtr->setNodeHeight(0);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() > nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getLeftPtr()->getNodeHeight() + 1);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() < nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getRightPtr()->getNodeHeight() + 1);
        }
    }
    else if (nNode->getData().getCancerRate() > nodePtr->getData().getCancerRate())
    {
        prevMove = lastMove;

        lastMove = 1; //Node is being inserted to the right

        insert(nodePtr->getRightPtr(), nNode, prevMove, lastMove);
        
        if ((nodePtr->getLeftPtr() == nullptr) && (nodePtr->getRightPtr() == nullptr))
        {
            nodePtr->setNodeHeight(0);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() > nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getLeftPtr()->getNodeHeight() + 1);
        }
        else if (nodePtr->getLeftPtr()->getNodeHeight() < nodePtr->getRightPtr()->getNodeHeight())
        {
            nodePtr->setNodeHeight(nodePtr->getRightPtr()->getNodeHeight() + 1);
        }
    }
    
    if (abs(nodePtr->getLeftPtr()->getNodeHeight() - nodePtr->getRightPtr()->getNodeHeight()) > 1)
    {
        if ((prevMove == 0) && (lastMove == 0)) //Left, left insertion
        {
            rotateLeft(nodePtr);
        }
        else if ((prevMove == 1) && (lastMove == 1)) //Right, right insertion
        {
            rotateRight(nodePtr);
        }
        else if ((prevMove == 0) && (lastMove == 1)) //Left, right insertion
        {
            rotateLeft(nodePtr);

            rotateRight(nodePtr);
        }
        else if ((prevMove == 1) && (lastMove == 0)) //Right, left insertion
        {
            rotateRight(nodePtr);

            rotateLeft(nodePtr);
        }
    }
}

void AVLTree::remove(AVLNode *&deleteNode, AVLNode *&nodePtr, int &prevMove, int &lastMove)
{
    if (deleteNode->getData().getCancerRate() < nodePtr->getData().getCancerRate())
    {
        prevMove = lastMove;

        lastMove = 0;

        remove(deleteNode, nodePtr->getLeftPtr(), prevMove, lastMove);
    }
    else if (deleteNode->getData().getCancerRate() > nodePtr->getData().getCancerRate())
    {
        prevMove = lastMove;

        lastMove = 1;

        remove(deleteNode, nodePtr->getRightPtr(), prevMove, lastMove);
    }
    else if (deleteNode->getData().getCancerRate() == nodePtr->getData().getCancerRate())
    {
        AVLNode *temp = nullptr;

        if (nodePtr == nullptr)
        {

        }
        else if (nodePtr->getRightPtr() == nullptr)
        {
            temp = nodePtr;

            nodePtr = nodePtr->getLeftPtr();

            delete temp;
        }
        else if (nodePtr->getLeftPtr() == nullptr)
        {
            temp = nodePtr;

            nodePtr = nodePtr->getRightPtr();

            delete temp;
        }
        else
        {
            temp = nodePtr->getRightPtr();

            while (temp->getLeftPtr())
            {
                temp = temp->getLeftPtr();
            }
            temp->setLeftPtr(nodePtr->getLeftPtr());

            temp = nodePtr;

            nodePtr = nodePtr->getRightPtr();

            delete temp;
        }
        
    }
    
    if (abs(nodePtr->getLeftPtr()->getNodeHeight() - nodePtr->getRightPtr()->getNodeHeight()) > 1)
    {
        if ((prevMove == 0) && (lastMove == 0)) //Left, left insertion
        {
            rotateLeft(nodePtr);
        }
        else if ((prevMove == 1) && (lastMove == 1)) //Right, right insertion
        {
            rotateRight(nodePtr);
        }
        else if ((prevMove == 0) && (lastMove == 1)) //Left, right insertion
        {
            rotateLeft(nodePtr);

            rotateRight(nodePtr);
        }
        else if ((prevMove == 1) && (lastMove == 0)) //Right, left insertion
        {
            rotateRight(nodePtr);

            rotateLeft(nodePtr);
        }
    }
}

void AVLTree::clear(AVLNode *nodePtr)
{
    if (nodePtr)
    {
        if (nodePtr->getLeftPtr())
        {
            clear(nodePtr->getLeftPtr());
        }
        if (nodePtr->getRightPtr())
        {
            clear(nodePtr->getRightPtr());
        }

        delete nodePtr;
    }
}

AVLNode* AVLTree::findMin(AVLNode *& nodePtr)
{
    if (nodePtr->getLeftPtr())
    {
        return findMin(nodePtr->getLeftPtr());
    }
    else
    {
        return nodePtr;
    }
    
}

AVLNode* AVLTree::findMax(AVLNode *& nodePtr)
{
    if (nodePtr->getRightPtr())
    {
        return findMin(nodePtr->getRightPtr());
    }
    else
    {
        return nodePtr;
    }
}

void AVLTree::printInOrder(AVLNode *nodePtr)
{
    if (nodePtr)
    {
        printInOrder(nodePtr);

        std::cout << nodePtr->getData().getCountryName() << nodePtr->getData().getCancerRate() << std::endl;

        printInOrder(nodePtr);
    }
}

AVLNode* AVLTree::getRoot()
{
    return root;
}

void AVLTree::setRoot(AVLNode *nRoot)
{
    root = nRoot;
}

void AVLTree::rotateLeft(AVLNode *&parentPtr)
{
    parentPtr->getRightPtr()->setData(parentPtr->getData());

    parentPtr = parentPtr->getLeftPtr();
}

void AVLTree::rotateRight(AVLNode *&parentPtr)
{
    parentPtr->getLeftPtr()->setData(parentPtr->getData());

    parentPtr = parentPtr->getRightPtr();
}*/