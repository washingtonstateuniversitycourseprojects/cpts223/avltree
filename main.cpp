/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 3                                              *
* February 29, 2020                                                     *
* Decription: This file contains the main function for the program.     *
* Worst case Big-O of insert = logn                                     *
* Explanation: The worst case of insert is that the new node will be    *
*              inserted at the bottom of the tree which requires        *
*              traversing the height of the tree, which is logn.        *
* Worst case Big-O of printInOrder = n                                  *
* Explanation: The worst case of printInOrder involves traversing the   *
*              entire tree, printing all n nodes, so Big O is n.        *
* Worst case Big-O of findMax = logn                                    *
* Explanation: The worst case of findMax is that the max node will be   *
*              at the bottom of the tree which requires traversing      *
*              the height of the tree, which is logn.                   *
************************************************************************/

#include "AVLTree.h"
#include "CancerData.h"


int main()
{

    AVLTree<CancerData>* womenTreePtr = new AVLTree<CancerData>; //Initialize AVLTree for women cancer data

    AVLTree<CancerData>* menTreePtr =  new AVLTree<CancerData>; //Initialize AVLTree for men cancer data

    AVLTree<CancerData>* bothTreePtr =  new AVLTree<CancerData>; //Initialize AVLTree for both cancer data

    std::ifstream womenInFile, menInFile, bothInFile;  //Initialize infiles for cancer data

    int count = 1; //Initialize count for printing in order

    bothInFile.open("bothcancerdata2018.csv"); //Open both cancer data file

    menInFile.open("mencancerdata2018.csv"); //Open men cancer data file

    womenInFile.open("womencancerdata2018.csv"); //Open women cancer data file

    bothTreePtr->loadData(bothInFile); //Populate both cancer data tree

    menTreePtr->loadData(menInFile); //Populate men cancer data tree

    womenTreePtr->loadData(womenInFile); //Populate women cancer data tree

    std::cout << "Both cancer data from most to least: " << std::endl;

    bothTreePtr->printBackwardsOrder(bothTreePtr->getRoot(), "Both", count); //Print in order from greatest to least

    std::cout << std::endl;

    count = 1; //Reset count for printing in order

    std::cout << "Men cancer data from most to least: " << std::endl;

    menTreePtr->printBackwardsOrder(menTreePtr->getRoot(), "Men", count); //Print in order from greatest to least

    std::cout << std::endl;

    count = 1; //Reset count for printing in order

    std::cout << "Women cancer data from most to least: " << std::endl;

    womenTreePtr->printBackwardsOrder(womenTreePtr->getRoot(), "Women", count); //Print in order from greatest to least

    std::cout << std::endl;

    std::cout << "Both highest cancer data: " << std::endl;

    std::cout <<"< Both: 1. " << bothTreePtr->findMax(bothTreePtr->getRoot()) << ">" << std::endl; //Find and print highest both cancer rate

    std::cout << "Both lowest cancer data: " << std::endl;

    std::cout << "< Both: 50. " << bothTreePtr->findMin(bothTreePtr->getRoot()) << ">" << std::endl; //Find and print lowest both cancer rate

    std::cout << std::endl;

    std::cout << "Men highest cancer data: " << std::endl;

    std::cout <<"< Men: 1. " << menTreePtr->findMax(menTreePtr->getRoot()) << ">" << std::endl; //Find and print highest men cancer rate

    std::cout << "Men lowest cancer data: " << std::endl;

    std::cout << "< Men: 50. " << menTreePtr->findMin(menTreePtr->getRoot()) << ">" << std::endl; //Find and print lowest men cancer rate

    std::cout << std::endl;

    std::cout << "Women highest cancer data: " << std::endl;

    std::cout <<"< Women: 1. " << womenTreePtr->findMax(womenTreePtr->getRoot()) << ">" << std::endl; //Find and print highest women cancer rate

    std::cout << "Women lowest cancer data: " << std::endl;

    std::cout << "< Women: 50. " << womenTreePtr->findMin(womenTreePtr->getRoot()) << ">" << std::endl; //Find and print lowest women cancer rate

    std::cout << std::endl;

    delete bothTreePtr; //Delete both cancer data tree

    delete menTreePtr; //Delete men cancer data tree

    delete womenTreePtr; //Delete women cancer data tree

    bothInFile.close(); //Delete both cancer data file

    menInFile.close(); //Delete men cancer data file

    womenInFile.close(); //Delete women cancer data file

    return 0;
}
