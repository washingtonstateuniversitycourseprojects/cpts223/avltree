/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 29, 2020                                                     *
* Decription: This file contains function definitions for the           *
*             CancerData class.                                         *
************************************************************************/

#include "CancerData.h"


/************************************************************************
* Programming Assignment 3                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object of type CancerData                  *
************************************************************************/

CancerData::CancerData()
{
    countryName = "";

    cancerRate = 0.0;
}


/************************************************************************
* Programming Assignment 3                                              *
* Standard constructor:                                                 *
*                                                                       *
* Description: Constructs an object of type CancerData using given      *
*              member variables.                                        *
************************************************************************/

CancerData::CancerData(std::string nCountryName, float nCancerRate)
{
    countryName = nCountryName;

    cancerRate = nCancerRate;
}


/************************************************************************
* Programming Assignment 3                                              *
* Copy constructor:                                                     *
*                                                                       *
* Description: Constructs an object of type CancerData using given      *
*              another object of type CancerData.                       *
************************************************************************/

CancerData::CancerData(CancerData& newObject)
{
    countryName = newObject.getCountryName();

    cancerRate = newObject.getCancerRate();
}


/************************************************************************
* Programming Assignment 3                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destructs object of type CancerData.                     *
************************************************************************/

CancerData::~CancerData()
{

}


/************************************************************************
* Programming Assignment 3                                              *
* GetCountryName:                                                       *
*                                                                       *
* Description: Returns the member variable countryName.                 *
************************************************************************/

std::string CancerData::getCountryName()
{
    return countryName;
}


/************************************************************************
* Programming Assignment 3                                              *
* GetCancerRate:                                                        *
*                                                                       *
* Description: Returns the member variable cancerRate.                  *
************************************************************************/

float CancerData::getCancerRate()
{
    return cancerRate;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetCountryName:                                                       *
*                                                                       *
* Description: Sets the member variable countryName.                    *
************************************************************************/

void CancerData::setCountryName(std::string nCountryName)
{
    countryName = nCountryName;
}


/************************************************************************
* Programming Assignment 3                                              *
* SetCancerRate:                                                        *
*                                                                       *
* Description: Sets the member variable cancerRate.                     *
************************************************************************/

void CancerData::setCancerRate(float nCancerRate)
{
    cancerRate = nCancerRate;
}
